import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { ProfileComponent } from './components/profile/profile.component';
import { RequestResetComponent } from './components/password/request-reset/request-reset.component';
import { LoggedInGuard } from './Guards/logged-in.guard';
import { AdminGuard } from './Guards/admin.guard';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { RoleComponent } from './components/role/role.component';
import { ProjectComponent } from './components/project/project.component';
import { CompanyComponent } from './components/company/company.component';
import { UsersComponent } from './components/users/users.component';
import { TaskComponent } from './components/task/task.component';
import { TaskoneComponent } from './components/taskone/taskone.component';


const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full',
  },
  {
    path:'login',
    component: LoginComponent
  },
  {
    path:'signup',
    component: UsersComponent,
    canActivate: [LoggedInGuard, AdminGuard]
  },
  {
    path:'profile',
    component: ProfileComponent,
    canActivate: [LoggedInGuard]
  },
  {
    path:'dashboard',
    component: DashboardComponent,
    canActivate: [LoggedInGuard]
  },
  {
    path:'role',
    component: RoleComponent,
    canActivate: [LoggedInGuard, AdminGuard],
  },
  {
    path:'company',
    component: CompanyComponent,
    canActivate: [LoggedInGuard, AdminGuard],
  },
  {
    path:'projects',
    component: ProjectComponent,
    canActivate: [LoggedInGuard, AdminGuard],
  },
  {
    path:'project/:id',
    component: TaskComponent,
    canActivate: [LoggedInGuard],
  },
  {
    path:'task/:id',
    component: TaskoneComponent,
    canActivate: [LoggedInGuard],
  },
 
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
