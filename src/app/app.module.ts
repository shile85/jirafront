import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { LoginComponent } from './components/login/login.component';
import { ProfileComponent } from './components/profile/profile.component';
import { RequestResetComponent } from './components/password/request-reset/request-reset.component';
import { ResponseResetComponent } from './components/password/response-reset/response-reset.component';
import { TokenInterceptorService } from './Services/token-interceptor.service';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { RoleComponent } from './components/role/role.component';
import { LayoutModule } from '@angular/cdk/layout';
import { ProjectComponent } from './components/project/project.component';
import { CompanyComponent } from './components/company/company.component';
import { UsersComponent } from './components/users/users.component';
import { TaskComponent } from './components/task/task.component';
import { TaskoneComponent } from './components/taskone/taskone.component';





@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    LoginComponent,
    ProfileComponent,
    RequestResetComponent,
    ResponseResetComponent,
    DashboardComponent,
    RoleComponent,
    ProjectComponent,
    CompanyComponent,
    UsersComponent,
    TaskComponent,
    TaskoneComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,

    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    LayoutModule,
   
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptorService,
      multi: true,
    }
  ],


  bootstrap: [AppComponent]
})
export class AppModule { }

