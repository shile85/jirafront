import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHandlerService } from 'src/app/Services/http-handler.service';
import { TokenService } from 'src/app/Services/token.service';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/Services/auth.service';
import { UsersService } from 'src/app/Services/users.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public form = {
    email : null,
    password : null,
  };

  public error = null;

  constructor(
    private httphandler: HttpHandlerService,
    private Token: TokenService,
    private Users: UsersService,
    private router: Router, 
    private Auth : AuthService,
    ) { }

  onSubmit(){
    this.httphandler.login(this.form).subscribe(
      data => this.handleResponse(data),
      error => this.handleError(error),
    );
  }

  handleResponse(data){
    this.Token.handle(data.access_token);
    this.Auth.changeAuthStatus(true);
    this.router.navigateByUrl('/profile');
    if(this.Users.getAuthUserId() == 1) this.Auth.changeAdminStatus(true);
  }

  handleError(error){
    this.error = error.error.error;
  }

  ngOnInit() {
  }

}
