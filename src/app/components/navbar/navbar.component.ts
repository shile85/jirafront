import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../Services/auth.service';
import { Router } from '@angular/router';
import { TokenService } from 'src/app/Services/token.service';
import { UsersComponent } from '../users/users.component';
import { UsersService } from 'src/app/Services/users.service';


@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {


  public loggedIn : boolean;
  public isAdmin :boolean;
  constructor(
    private Auth : AuthService,
    private router : Router,
    private Token : TokenService,
    private Users : UsersService,
  ) { }

  ngOnInit() {
    this.Auth.authStatus.subscribe(value => this.loggedIn = value);
    this.Auth.adminStatus.subscribe(value => this.isAdmin = value);
    if((this.Users.getAuthUserId() == 1) || (this.Users.getAuthUserId() == 5)) this.Auth.changeAdminStatus(true);
  }

  
  

  logout(event : MouseEvent){
    event.preventDefault();
    this.Token.remove();
    this.Auth.changeAuthStatus(false);
    this.router.navigateByUrl('/login');
    this.Auth.changeAdminStatus(false);
  }

}
