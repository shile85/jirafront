import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AuthService } from 'src/app/Services/auth.service';
import { UsersService } from 'src/app/Services/users.service';
import { HttpHandlerService } from 'src/app/Services/http-handler.service';

@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.css']
})
export class TaskComponent implements OnInit {

  private form = {
    taskId : null,
    taskName : null,
    description : null,
    status : 1,
    userId : null,
    sprintId : null,
    points : null,
  }

  private id;
  private isAdmin;
  private error;
  private project;
  private users;
  private sprints;
  private tasks;
  private tasksMod;
  private task;
  private editHidden = null;
  private addHidden = null;
  private tableHidden = null;

  constructor(
    private route: ActivatedRoute,
    private Auth: AuthService,
    private Users: UsersService,
    private httphandler : HttpHandlerService,
  ) { }


  showAddTask(){
    this.addHidden = false;
  }

  showEditTask(event : MouseEvent, id){
    this.editHidden = false;
    this.httphandler.editTaskData(id).subscribe(
      response=>{
        this.task = response;
    },
    error=>{
      this.handleError(error);
    })
  }
  onEditSubmit(id){
    this.form.taskId = id;
    console.log(id);
    this.httphandler.updateTaskData(this.form).subscribe(
      data => this.handleResponse(data),
      error => this.handleError(error)
    );
    this.ngOnInit();
  }

  onSubmit(){
    this.httphandler.addTask(this.form).subscribe(
      data => this.handleResponse(data),
      error => this.handleError(error)
    );
    this.ngOnInit();
  }

  deleteTask(event : MouseEvent, id){
    this.httphandler.deleteTask(id).subscribe(
      response=>{
        data => this.handleResponse(data);
        this.ngOnInit();
    },
    error=>{
      this.handleError(error);
    })
  }

  handleResponse(data){
    this.ngOnInit();
  }

  handleError(error){
    console.log(error);
    this.error = error;
  }

  ngOnInit() {
    this.editHidden = true;
    this.addHidden = true;
    this.id = this.route.snapshot.paramMap.get('id');
    console.log(this.id);
    this.httphandler.projectTasksPage(this.id).subscribe(
      response=>{
        this.project = response[0];
        this.users = response[1];
        this.sprints = response[2];
        this.tasks = response[3];
        this.tasksMod = response[4];
    },
    error=>{
      this.handleError(error)
    })
  }

}
