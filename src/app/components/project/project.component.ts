import { Component, OnInit } from '@angular/core';
import { HttpHandlerService } from 'src/app/Services/http-handler.service';
import { TokenService } from 'src/app/Services/token.service';
import { AuthService } from 'src/app/Services/auth.service';
import { UsersService } from 'src/app/Services/users.service';


@Component({
  selector: 'app-project',
  templateUrl: './project.component.html',
  styleUrls: ['./project.component.css']
})
export class ProjectComponent implements OnInit {

  
  public form = {
    projectName : null,
    projectId : null,
    company : null,
    start : null,
    projectManager : null,
    users : [],
    sprintLenght : null,
    sprintPoints : null
  }

  public isAdmin : boolean; 
  public error;
  private projects;
  private project;
  private projectsMod;
  private users;
  private companies;
  private company;
  private hidden = null;


  constructor(
    private httphandler: HttpHandlerService,
    private Auth : AuthService,
    private Users : UsersService

  ) { }

  onChangeCategory(users: any[]){ 
    users.forEach(user => {
      this.form.users.push(user.id);
    });
  }

  showAddProject(){
    this.hidden = false;
  }
  
  onSubmit(){
    console.log(this.form);
    var results = [];
    for (var i=0; i < this.form.users.length; i++ ){
      if ( this.form.users[i] == true ){
          results.push( i );
      }
    }
    this.form.users = results;
    this.httphandler.addProject(this.form).subscribe(
      data => this.handleResponse(data),
      error => this.handleError(error)
    );
  }

  showEditProject(event : MouseEvent, id){
    this.httphandler.editProjectData(id).subscribe(
      response=>{
        this.project = response;
        this.hidden = true;
    },
    error=>{
      this.handleError(error);
    })
  }

  editProject(id){
    this.form.projectId = id;
    this.httphandler.updateProjectData(this.form).subscribe(
      data => this.handleResponse(data),
      error => this.handleError(error)
    );
  }

  deleteProject(event : MouseEvent, id){
    this.httphandler.deleteProject(id).subscribe(
      response=>{
          this.ngOnInit();
    },
    error=>{
      this.handleError(error);
    })
  }

  handleResponse(data){
    this.ngOnInit();
    this.hidden = false;
  }

  handleError(error){
    console.log(error);
    this.error = error;
  }


  ngOnInit() {
    this.Auth.adminStatus.subscribe(value => this.isAdmin = value);
    if(this.Users.getAuthUserId() == 1) this.Auth.changeAdminStatus(true);
    this.httphandler.projectPage().subscribe(
      response=>{
      this.users = response[0];
      this.companies = response[1];
      this.projects = response[2];
      this.projectsMod = response[3];
      this.form.projectName = null;
      this.form.projectId = null,
      this.form.company = null,
      this.form.start = null,
      this.form.projectManager = null,
      this.form.users = []
    },
    error=>{
      this.handleError(error)
    })
  }

}
