import { Component, OnInit } from '@angular/core';
import { HttpHandlerService } from 'src/app/Services/http-handler.service';
import { TokenService } from 'src/app/Services/token.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  public form = {
    userId : null,
    firstName : null,
    lastName : null,
    email : null,
    password : null,
    password_confirmation : null,
    username : null,
    parent : null,
    role : null,
    userstatus : null

  }

  private users;
  private user;
  private roles;
  private usersMod;
  private hidden;
  private hiddenAdd;
  private hiddenEdit;
  public error = [];

  constructor(
    private httphandler: HttpHandlerService,
    private Token : TokenService,
    private router : Router,
  ) { }
  showEmployees(event : MouseEvent){
    this.ngOnInit();
  }
  showAddEmployee(event : MouseEvent){
    this.hidden = true;
    this.hiddenEdit = true;
    this.hiddenAdd= false;
  }

  onSubmit(){
    console.log(this.form);
    this.httphandler.signup(this.form).subscribe(
      data => this.handleResponse(data),
      error => this.handleError(error)
    );
  }
  showEditUser(event : MouseEvent, id){
    this.httphandler.editUserData(id).subscribe(
      response=>{
        this.user = response;
        this.hidden = true;
        this.hiddenEdit = false;
    },
    error=>{
      this.handleError(error);
    })
  }

  onEditSubmit(id){
    this.form.userId = id;
    this.httphandler.updateUserData(this.form).subscribe(
      data => this.handleResponse(data),
      error => this.handleError(error)
    );
  }

  deleteUser(event : MouseEvent, id){
    this.httphandler.deleteUser(id).subscribe(
      response=>{
          this.ngOnInit();
    },
    error=>{
      this.handleError(error);
    })
  }

  handleResponse(data){
    this.ngOnInit();
  }

  handleError(error){
    this.error = error.error.errors;
  }

  ngOnInit() {
    this.httphandler.users().subscribe(
      response=>{
      this.form = {
        userId : null,
        firstName : null,
        lastName : null,
        email : null,
        password : null,
        password_confirmation : null,
        username : null,
        parent : null,
        role : null,
        userstatus : null
      }
      this.hidden = false;
      this.hiddenAdd = true;
      this.hiddenEdit = true;

      this.users = response[0];
      this.usersMod = response[1];
      this.roles = response[2];
    },
    error=>{
      console.log(error);
    }
    );
    
  }

}
