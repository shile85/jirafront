import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpHandlerService } from 'src/app/Services/http-handler.service';

@Component({
  selector: 'app-taskone',
  templateUrl: './taskone.component.html',
  styleUrls: ['./taskone.component.css']
})
export class TaskoneComponent implements OnInit {


  public form = {
    statusName : null,
    status : null,
    taskId : null,
  }
  private id;
  private task;
  private hidden = true;
  private error;
  private statuses;

  constructor(
    private route: ActivatedRoute,
    private httphandler : HttpHandlerService
  ) { }


  getTaskDataMod(){
    this.id = this.route.snapshot.paramMap.get('id');
    this.httphandler.getTaskByIdMod(this.id).subscribe(
      response=>{
        this.task = response;
      },
      error=>{
        console.log(error);
      }
      );
  }

  showChangeStatus(event : MouseEvent){
    this.httphandler.showChangeStatus().subscribe(
      response=>{
        this.statuses = response;
        this.hidden = false;
        this.ngOnInit();
    },
    error=>{
      this.handleError(error);
    })
  }

  onSubmit(id){
    this.form.taskId = id;
    this.httphandler.updateStatus(this.form).subscribe(
      data => this.handleResponse(),
      error => this.handleError(error)
    );
  }

  handleError(error){
    console.log(error);
    this.error = error;
  }

  handleResponse(){
    this.ngOnInit();
    this.hidden = true;
    
  }

  ngOnInit() {
    this.getTaskDataMod();
  }

  

}
