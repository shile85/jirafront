import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/Services/auth.service';
import { UsersService } from 'src/app/Services/users.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  
  public isAdmin : boolean; 

  constructor(
    private Auth : AuthService,
    private Users : UsersService
  ) { }

  ngOnInit() {
    this.Auth.adminStatus.subscribe(value => this.isAdmin = value);
    if(this.Users.getAuthUserId() == 1) this.Auth.changeAdminStatus(true);
    
  }

}
