import { Component, OnInit } from '@angular/core';
import { TokenService } from '../../Services/token.service';
import { HttpHandlerService } from '../../Services/http-handler.service';
import { UsersService } from 'src/app/Services/users.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  private authUserId;
  private user;
  private tasks;
  private projects;

  constructor(
    private token : TokenService,
    private httphandler: HttpHandlerService,
    private Users : UsersService

  ) { }

  ngOnInit() {
    this.getAuthUserData();
    this.getAuthProjects();
    this.getAuthTasks();
  }

  getAuthProjects(){
    this.authUserId = this.Users.getAuthUserId();
    this.httphandler.getAuthProjects(this.authUserId).subscribe(
      response=>{
        this.projects = response;
      },
      error=>{
        console.log(error);
      }
      );
  }

  getAuthTasks(){

    this.authUserId = this.Users.getAuthUserId();
    this.httphandler.getAuthTasks(this.authUserId).subscribe(
      response=>{
        this.tasks = response;
      },
      error=>{
        console.log(error);
      }
      );

  }

  getAuthUserData(){
    this.authUserId = this.Users.getAuthUserId();
    this.httphandler.getAuthUserData(this.authUserId).subscribe(
      response=>{
        this.user = response;
      },
      error=>{
        console.log(error);
      }
      );
  }



}
