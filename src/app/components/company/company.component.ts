import { Component, OnInit } from '@angular/core';
import { HttpHandlerService } from 'src/app/Services/http-handler.service';
import { NgForOf } from '@angular/common';
import { TokenService } from 'src/app/Services/token.service';

@Component({
  selector: 'app-company',
  templateUrl: './company.component.html',
  styleUrls: ['./company.component.css']
})
export class CompanyComponent implements OnInit {

  public form = {
    companyName : null,
    companyId : null,
    companyStatus : null
  }

  public error;
  private companies;
  private company;
  private hidden = null;
  
  
  constructor(
    private httphandler: HttpHandlerService,
    private Token : TokenService,
  ) { }

  showAddCompany(event : MouseEvent){
    this.hidden = false;
  }

  onSubmit(){
    console.log(this.form);
    this.httphandler.addCompany(this.form).subscribe(
      data => this.handleResponse(),
      error => this.handleError(error)
    );
  }

  showEditCompany(event : MouseEvent, id){
    console.log(id);
    this.httphandler.editCompanyData(id).subscribe(
      response=>{
        this.company = response;
        this.hidden = true;
        this.ngOnInit();
    },
    error=>{
      this.handleError(error);
    })
  }

  editCompany(id){
    this.form.companyId = id;
    this.httphandler.editCompany(this.form).subscribe(
      data => this.handleResponse(),
      error => this.handleError(error)
    );
  }

  deleteCompany(event : MouseEvent, id){
    this.httphandler.deleteCompany(id).subscribe(
      response=>{
        this.handleResponse();
    },
    error=>{
      this.handleError(error);
    })
  }

  handleResponse(){
    this.ngOnInit();
    this.hidden = false;
    
  }

  handleError(error){
    console.log(error);
    this.error = error;
  }

  ngOnInit() {
    this.httphandler.companyPage().subscribe(
      response=>{
      data =>  this.Token.handle(data.access_token);
      this.companies = response;
      console.log(this.companies);
      this.form.companyName = null;
    },
    error=>{
      this.handleError(error)
    })
  }

}
