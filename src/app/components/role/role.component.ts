import { Component, OnInit } from '@angular/core';
import { HttpHandlerService } from 'src/app/Services/http-handler.service';

@Component({
  selector: 'app-role',
  templateUrl: './role.component.html',
  styleUrls: ['./role.component.css']
})
export class RoleComponent implements OnInit {


  public form = {
    roleName : null,
    roleId : null
  }

  public error;
  private roles;
  private role;
  private hidden = null;
  
  
  constructor(
    private httphandler: HttpHandlerService,
  ) { }

  showAddRole(event : MouseEvent){
    this.hidden = false;
  }

  onSubmit(){
    this.httphandler.addRole(this.form).subscribe(
      data => this.handleResponse(),
      error => this.handleError(error)
    );
  }

  showEditRole(event : MouseEvent, id){
    this.httphandler.editRoleData(id).subscribe(
      response=>{
        this.role = response;
        this.hidden = true;
        this.ngOnInit();
    },
    error=>{
      this.handleError(error);
    })
  }

  editRole(id){
    this.form.roleId = id;
    this.httphandler.editRole(this.form).subscribe(
      data => this.handleResponse(),
      error => this.handleError(error)
    );
  }

  deleteRole(event : MouseEvent, id){
    this.httphandler.deleteRole(id).subscribe(
      response=>{
        this.handleResponse();
    },
    error=>{
      this.handleError(error);
    })
  }

  handleResponse(){
    this.ngOnInit();
    this.hidden = false;
    
  }

  handleError(error){
    console.log(error);
    this.error = error;
  }

  ngOnInit() {
    this.httphandler.rolePage().subscribe(
      response=>{
      this.roles = response;
      this.form.roleName = null;
    },
    error=>{
      this.handleError(error)
    })
  }

  
  

  

  

  

}
