import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { TokenService } from './token.service';

@Injectable({
  providedIn: 'root'
})
export class HttpHandlerService {

  private baseUrl = 'http://jiraback.me/api';

  constructor(
    private http:HttpClient,
    private token : TokenService
    ) { }

  // Auth
  

  login(data){
    return this.http.post(`${this.baseUrl}/login`, data)
  }

  // Users
  users(){
    var data =  this.http.get(`${this.baseUrl}/usersPage`);
    return data;
  }
  getUserById(id){
    var data =  this.http.get(`${this.baseUrl}/usersPage/${id}`,id);
    return data;
  }
  getUserRoleData(id){
    var data =  this.http.get(`${this.baseUrl}/userRoleData/${id}`,id);
    return data;
  }
  editUserData(id){
    return this.http.post(`${this.baseUrl}/editUserData/${id}`,id);
  }
  updateUserData(data){
    return this.http.post(`${this.baseUrl}/updateUserData`,data);
  }
  signup(data){
    return this.http.post(`${this.baseUrl}/signup`, data)
  }
  deleteUser(id){
    return this.http.post(`${this.baseUrl}/deleteUser/${id}`,id);
  }
  getAuthUserData(id){
    var data =  this.http.get(`${this.baseUrl}/getAuthUserData/${id}`);
    return data;
  }
  

   // Role
   rolePage(){
    var data =  this.http.get(`${this.baseUrl}/rolePage`);
    return data;
  }
   addRole(data){
    return this.http.post(`${this.baseUrl}/addRole`, data)
  }
  editRoleData(id){
    return this.http.post(`${this.baseUrl}/editRoleData/${id}`,id);
  }
  editRole(data){
    return this.http.post(`${this.baseUrl}/editRole`, data)
  }
  deleteRole(id){
    return this.http.post(`${this.baseUrl}/deleteRole/${id}`,id);
  }

  // Company

  companyPage(){
    var data =  this.http.get(`${this.baseUrl}/companyPage`);
    return data;
  }
   addCompany(data){
    return this.http.post(`${this.baseUrl}/addCompany`, data)
  }
  editCompanyData(id){
    return this.http.post(`${this.baseUrl}/editCompanyData/${id}`,id);
  }
  editCompany(data){
    return this.http.post(`${this.baseUrl}/editCompany`, data)
  }
  deleteCompany(id){
    return this.http.post(`${this.baseUrl}/deleteCompany/${id}`,id);
  }

  // Project

  projectPage(){
    var data =  this.http.get(`${this.baseUrl}/projectPage`);
    return data;
  }
  addProject(data){
    return this.http.post(`${this.baseUrl}/addProject`, data)
  }
  editProjectData(id){
    return this.http.post(`${this.baseUrl}/editProjectData/${id}`,id);
  }
  updateProjectData(data){
    return this.http.post(`${this.baseUrl}/updateProjectData`,data);
  }
  deleteProject(id){
    return this.http.post(`${this.baseUrl}/deleteProject/${id}`,id);
  }
  getAuthProjects(id){
    var data =  this.http.get(`${this.baseUrl}/getAuthProjects/${id}`);
    return data;
  }

  

  // Task
  projectTasksPage(id){
    return this.http.get(`${this.baseUrl}/projectTasksPage/${id}`,id);
  }
  addTask(data){
    return this.http.post(`${this.baseUrl}/addTask  `, data)
  }
  editTaskData(id){
    return this.http.post(`${this.baseUrl}/editTaskData/${id}`,id);
  }
  updateTaskData(data){
    return this.http.post(`${this.baseUrl}/updateTaskData`,data);
  }
  deleteTask(id){
    return this.http.post(`${this.baseUrl}/deleteTask/${id}`,id);
  }
  getAuthTasks(id){
    var data =  this.http.get(`${this.baseUrl}/getAuthTasks/${id}`);
    return data;
  }
  getTaskByIdMod(id){
    var data =  this.http.get(`${this.baseUrl}/getTaskByIdMod/${id}`);
    return data;
  }
  showChangeStatus(){
    var data =  this.http.get(`${this.baseUrl}/showChangeStatus`);
    return data;
  }
  updateStatus(data){
    console.log(data);
    return this.http.post(`${this.baseUrl}/updateStatus`,data);
  }
}
