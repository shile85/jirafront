import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs'
import { TokenService } from './token.service';
import { UsersService } from './users.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private loggedIn = new BehaviorSubject<boolean>(this.Token.loggedIn());
  authStatus = this.loggedIn.asObservable();

  private isAdmin = new BehaviorSubject<boolean>(this.Users.isAdmin());
  adminStatus = this.isAdmin.asObservable();

  changeAuthStatus(value: boolean){
    this.loggedIn.next(value);
  }
  changeAdminStatus(value: boolean){
    this.isAdmin.next(value);
  }

  constructor(
    private Token : TokenService,
    private Users : UsersService
  ) { }

  

}
