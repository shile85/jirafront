import { Injectable } from '@angular/core';
import { TokenService } from './token.service';
import * as jwt_decode from "jwt-decode";

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  

  constructor(
    private token : TokenService,
  ) { }

  isAdmin(){
    if((this.getAuthUserId() == 1) || (this.getAuthUserId() == 5)) {
      return true;
    }
    return false;
  }

  getAuthUserId(){
    
    var userId = this.getDecodedAccessToken(this.token.get());
    if(userId) return userId.sub;
    
  }

  getDecodedAccessToken(token: string): any {
    try{
        return jwt_decode(token);
    }
    catch(Error){
        return null;
    }
  }


}
