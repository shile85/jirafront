import { Injectable } from '@angular/core';


@Injectable({
  providedIn: 'root'
})
export class TokenService {

  constructor() { }

  private iss = {
    login : 'http://jiraback.me/api/login',
    signup: 'http://jiraback.me/api/signup',
    addRole: 'http://jiraback.me/api/addRole'
  }

  handle(token){
    this.set(token);
  }

  set(token){
    localStorage.setItem('token', token);
  }

  get(){
    return localStorage.getItem('token');
  }

  remove(){
    localStorage.removeItem('token');
  }
  
  isValid(){
    const token = this.get();
    if(token){
      const payload = this.payload(token);
      if(payload){
        return Object.values(this.iss).indexOf(payload.iss) > -1 ? true : false;
      }
    }

    return false;
  }

  payload(token){
    const payload = token.split('.')[1];
    return this.decode(payload);
  }

  decode(payload){
    const dec = JSON.parse(atob(payload))
    return dec;
    
  }

  loggedIn(){
    return this.isValid();
  }

  
  

}
